var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	less = require('gulp-less'),
	jade = require('gulp-jade'),
	browserSync = require('browser-sync').create(),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	svgstore = require('gulp-svgstore'),
	svgmin = require('gulp-svgmin'),
	cheerio = require('gulp-cheerio');

var path = {
	html: {
		src:  ['./source/**/*.jade', '!./source/blocks/**/*.jade'],
		dest: './static'
	},
	css: {
		src: './source/less/style.less',
		dest: './static/css',
		watch: './source/less/**/*.less'
	},
	js: {
		src: './source/js/**/*.js',
		dest: './static/js'
	},
	images: {
		src: './source/images/**/*.{jpg,jpeg,png,gif,svg}',
		dest: './static/images'
	},
	ajax: {
		src: './source/ajax/**/*',
		dest: './static/ajax'
	},
	svg: {
		src: './source/images/svg/*.svg'
	}
}


gulp.task('browser-sync', function() {
    browserSync.init({
		open: true,
		server: { baseDir: './static' }
	});
});

gulp.task('jade', function(){
	gulp.src(path.html.src)
	.pipe(jade({
		basedir: './source',
		pretty: true
	}))
	.pipe(gulp.dest(path.html.dest))
	.pipe(browserSync.stream());
});

gulp.task('less', function(){
	gulp.src(path.css.src)
	.pipe(less())
	.pipe(autoprefixer({
		browsers: ['last 2 version', '> 2%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'android 4'],
		cascade: false
	}))
	.pipe(gulp.dest(path.css.dest))
	.pipe(browserSync.stream());
});

gulp.task('js', function(){
	gulp.src(path.js.src)
	.pipe(gulp.dest(path.js.dest))
	.pipe(browserSync.stream());
});

gulp.task('copy', function(){
	gulp.src('./source/fonts/**/*{.woff,.eot,.woff2,.ttf}')
	.pipe(gulp.dest('./static/fonts'))
	.pipe(browserSync.stream());

	gulp.src('./source/ajax/**/')
	.pipe(gulp.dest('./static/ajax'))
	.pipe(browserSync.stream());
});

gulp.task('images', function () {
    gulp.src(path.images.src)
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()]
    }))
    .pipe(gulp.dest(path.images.dest))
    .pipe(browserSync.stream());
});

gulp.task('svgstore', function () {
   gulp.src(path.svg.src)       
        .pipe(svgmin(function (file) {          
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: 'svg-',
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore())
		.pipe(cheerio({
            run: function ($) {				
                $('svg').attr('style',  'display:none');
            },
            parserOptions: { xmlMode: true }
        }))
        .pipe(gulp.dest('./source/blocks/'));
});

gulp.task('build', ['copy', 'jade', 'less', 'js', 'images', 'svgstore']);

gulp.task('default', ['build', 'browser-sync'], function () {
    gulp.watch('source/**/*.jade', ['jade']);
    gulp.watch(path.css.watch, ['less']);
    gulp.watch(path.images.src, ["images"]);
    gulp.watch(path.js.src, ['js']);
    gulp.watch(path.svg.src, ['svg']);
	gulp.watch(path.ajax.src, ['copy']);
});