// preloader
function showPreloader() {
    $('.c-preloader').show();
}

function hidePreloader() {
    $('.c-preloader').hide();
}

// function sendFormbyAjax(form){
//
//     var url = form.attr('action');
//
//     showPreloader();
//
//     $.ajax({
//         url: url,
//         data: form.serialize(),
//         success: function (html) {//
//         },
//         error: function (er) {
//             console.log(er);
//         },
//         complete: function(){
//             hidePreloader();
//         }
//     });
// }

function initCustomValidate(){
    $.validate({
        validateOnBlur: false,
        errorMessagePosition: 'bottom',
        scrollToTopOnError: false,
        language: validatorLanguage,
        borderColorOnError: '#e41919',
        onSuccess : function($form){
            //send form by ajax

            // if($form.hasClass('c-ajaxForm')){
            // sendFormbyAjax($form);

            // return false;
            // }
        }
    });
}

var validatorLanguage = {
    errorTitle: 'Ошибка отправки формы!',
    requiredFields: 'Необходимо заполнить поле',
    badTime: 'Введите корректное время',
    badEmail: 'Введите корректный e-mail адрес',
    badTelephone: 'Введите корректный номер телефона',
    badSecurityAnswer: 'Ответ на секретный вопрос не верен',
    badDate: 'Введите корректную дату',
    lengthBadStart: 'Значение должно быть между ',
    lengthBadEnd: ' знаками',
    lengthTooLongStart: 'Значение поля больше, чем ',
    lengthTooShortStart: 'Значение поля меньше, чем ',
    notConfirmed: 'Input values could not be confirmed',
    badDomain: 'Введите корректное доменное имя',
    badUrl: 'Значение поля не является корректным url-адресом',
    badCustomVal: 'Зачение поля не верно',
    andSpaces: ' и пробелов ',
    badInt: 'Поле может содержать только цифры',
    badSecurityNumber: 'Your social security number was incorrect',
    badUKVatAnswer: 'Incorrect UK VAT Number',
    badStrength: 'The password isn\'t strong enough',
    badNumberOfSelectedOptionsStart: 'You have to choose at least ',
    badNumberOfSelectedOptionsEnd: ' answers',
    badAlphaNumeric: 'Поле может содержать только буквы ',
    badAlphaNumericExtra: ' и ',
    wrongFileSize: 'Максимальный размер файла %s',
    wrongFileType: 'Разрешениы следующие форматы файлов: %s',
    groupCheckedRangeStart: 'Выберите между ',
    groupCheckedTooFewStart: 'Выберите не меньше, чем ',
    groupCheckedTooManyStart: 'Выберите не больше, чем ',
    groupCheckedEnd: ' item(s)',
    badCreditCard: 'The credit card number is not correct',
    badCVV: 'The CVV number was not correct',
    wrongFileDim: 'Incorrect image dimensions,',
    imageTooTall: 'the image can not be taller than',
    imageTooWide: 'the image can not be wider than',
    imageTooSmall: 'the image was too small',
    min: 'мин.',
    max: 'макс.',
    imageRatioNotAccepted: 'Image ratio is not accepted'
};

// popups
var popupModule = (function () {
    var module = {};
    var bPopup;

    module.init = function (elem, url, html, callbackFn) {
        elem.removeClass('catalog interior login registration transition');

        var loadfunction,
            options = {
                closeClass: 'c-popup-close',
                contentContainer: '.popup__content',
                modalColor: '#000000',
                opacity: 0.4,
                modalClose: true,
                onClose: function () {
                    $('.popup__content').empty();
                }
            };

        if( html && html.length) {

            loadfunction =  function () {
                $('.popup__content').html(html);
            }
            options.onOpen = loadfunction;
        } else{
            options.loadUrl = url;
            options.content = 'ajax';
        }

        bPopup = elem.bPopup(options, callbackFn);
    }

    module.close = function(){
        if(bPopup) {
            bPopup.close();
        }
    }

    return module;
})();

function displayPopup() {
    $('body').on('click', '.c-popup-link', function(e) {
        e.preventDefault();

        //catalog
        if($(this).hasClass('catalog')) {
            popupModule.init($('.c-popup'), $(this).attr('href'), undefined, function () {
                if ( !Modernizr.csscolumns ) {
                    $('.c-columns').multicolumn();
                }
            });

            $('.c-popup').addClass('catalog');

            if($(this).hasClass('c-interior')) {
                $('.c-popup').addClass('interior');
            }
            displayNavigationBl();

        //login
        } else if($(this).hasClass('login')) {
            popupModule.init($('.c-popup'), $(this).attr('href'));
            $('.c-popup').addClass('login');
            displayLoginRegistrationBl();
        }
    });
}

function displayNavigationBl() {
    $('body').on('mouseenter','.c-nav-link', function() {
        var currentLink = $(this);
        currentLink.addClass('active');
        $('.c-nav-link').not(currentLink).removeClass('active');
    });
}

function displayLoginRegistrationBl() {
    $('body').on('click','.c-login-link', function(e) {
        e.preventDefault();

        if($(this).hasClass('c-reg')) {
            $('.c-popup').addClass('registration transition');
        } else {
            $('.c-popup').removeClass('registration');
        }

    });
}

function displayDropdown() {
    $('body').on('click', '.c-dropdown-link', function (e) {
        e.preventDefault();

        var dropdown = $(this).closest('.c-dropdown');
        dropdown.toggleClass('open');
    });
}

function clearInput() {
    $('body').on('click', '.c-clear-input',function () {
        $(this).closest('.c-wrap-input').find('input').val('');
        $(this).addClass('hidden');
    });
}

function displayClearInputIcon() {
    $('body').on('change', '.c-filter-input', function () {
        var clearInputIcon =  $(this).parent().find('.c-clear-input');
        if($(this).val().length) {
            clearInputIcon.removeClass('hidden');
        }  else {
            clearInputIcon.addClass('hidden');
        }
    });
}

function displayFilterLink() {
    $('body').on('click', '.c-filter-show-more', function (e) {
        e.preventDefault();
        
        $(this).toggleClass('active');
        var hiddenLink = $(this).parent().find('.c-filter-link');
        if($(this).hasClass('active')) {

            hiddenLink.each(function () {
                $(this).removeClass('hidden');
            });

        } else {
            hiddenLink.each(function () {
                $(this).addClass('hidden');
            });
        }

    });
}

function showCurrency() {
    $('body').on('click', '.c-select-currency', function (e) {
        e.preventDefault();

        var yen = $('.c-yen'),
            rouble = $('.c-ruble'),
            dollar = $('.c-dollar'),
            current = $('.c-currency');

        
        if($(this).hasClass('yen')) {
            yen.removeClass('hidden');
            rouble.addClass('hidden');
            dollar.addClass('hidden');
            current.html($(this).html());

        } else if($(this).hasClass('ruble')) {
            rouble.removeClass('hidden');
            yen.addClass('hidden');
            dollar.addClass('hidden');
            current.html($(this).html());

        } else {
            dollar.removeClass('hidden');
            yen.addClass('hidden');
            rouble.addClass('hidden');
            current.html($(this).html());
        }
    });    
}

function displayTabs() {
    $('body').on('click', '.c-tab-link', function (e) {
        e.preventDefault();

        var $link = $(this);
        $link.addClass('active').siblings().removeClass('active');

        var	$content = $('.c-tabs-item');
        $content.eq($link.index()).addClass('active').siblings().removeClass('active');         
    });
}

$(function () {
    //Setup form validation on all forms
    initCustomValidate();
    
    displayPopup();

    displayDropdown();

    displayClearInputIcon();

    clearInput();

    displayFilterLink();

    showCurrency();

    displayTabs();
});
